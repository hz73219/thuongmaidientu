﻿namespace Ql_Ban_Hang.Security
{
    public class Confi
    {
        protected string Issuser { get; set; }
        protected string Audience { get; set; }
        protected string Key { get; set; }

        public Confi()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var config = builder.Build();
            var section = config.GetSection("Jwt");

            this.Issuser = section["Issuser"];
            this.Audience = section["Audience"];
            this.Key = section["Key"];
        }
    }
}
