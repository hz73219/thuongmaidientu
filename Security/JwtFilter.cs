﻿using Azure;
using Microsoft.AspNetCore.Builder.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Website_thoi_trang.Security;

namespace Ql_Ban_Hang.Security
{
    public class JwtFilter:Confi
    {
      
        // mã hóa mật khẩu
        public string HashPassword(string password)
        {
            if (password != null)
            {
                var saltBytes = Encoding.ASCII.GetBytes(Key);
                var passwordBytes = Encoding.ASCII.GetBytes(password);
                // Tạo khóa bí mật
                var hmac = new HMACSHA256(saltBytes);
                // Tạo mã băm
                var hash = hmac.ComputeHash(passwordBytes);
                // Chuyển đổi mã băm thành chuỗi hex
                var hexHash = BitConverter.ToString(hash).Replace("-", "");
                return hexHash;
            }
            return "";
           
        }
        //Xác thực mật khẩu
        public bool VerifyPassword(string password, string hashedPassword)
        {
            string passwordInput = HashPassword(password);
            if (passwordInput == hashedPassword)
            {
                return true;
            }
            else
            { return false; }
        }
        //Tạo chuỗi token
        public string GenerateJwtToken(string userId, string Role)
        { 
            var SecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));

            // Tạo các thông tin cho token
            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Name, userId),
                new Claim(JwtRegisteredClaimNames.Typ, Role)
            };
            var expires = DateTime.UtcNow.AddDays(7);
            var signingCredentials = new SigningCredentials(SecurityKey, SecurityAlgorithms.HmacSha256);
            // Tạo token
            var token = new JwtSecurityToken(
                issuer: Issuser,
                audience: Audience,
                claims: claims,
                expires: expires,
                signingCredentials: signingCredentials);
            // Mã hóa token thành chuỗi
            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);
            return tokenString;
        }
        public UserRole decryptionJwtToken(string tokenString)
        {
            // Chuỗi key bí mật được sử dụng để mã hóa token 
            var SecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));
            // Thiết lập các thông tin cho xác thực token
            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = Issuser,
                ValidateAudience = true,
                ValidAudience = Audience,
                ValidateLifetime = true,
                IssuerSigningKey = SecurityKey
            };
            // Giải mã token
            var handler = new JwtSecurityTokenHandler();
            ClaimsPrincipal user = handler.ValidateToken(tokenString, validationParameters, out var validatedToken);
            // Lấy thông tin user từ token
            var userId = user.FindFirst(JwtRegisteredClaimNames.Name)?.Value;
            var roleName = user.FindFirst(JwtRegisteredClaimNames.Typ)?.Value;
            if(userId!=null&&roleName!=null)
            {
                UserRole userRole = new UserRole(userId,roleName);
                return userRole;

            }
                UserRole userRole2 = new UserRole("0", "0");
                return userRole2;

        }
    }
}
