﻿namespace Website_thoi_trang.Security
{
    public class UserRole
    {
        public string userId { get; set; }
        public string roleName { get; set; }
        public UserRole (string userId, string roleName )
        {
            this.userId = userId;
            this.roleName = roleName;
        }
        public UserRole()
        {
            this.userId = "0";
            this.roleName = "0";
        }
    }
}
