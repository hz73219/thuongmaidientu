﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Ql_Ban_Hang.Models;
using Ql_Ban_Hang.Security;
using System.Text.Json;

namespace Ql_Ban_Hang.Controllers
{
    public class ProfileController : BaseController
    {
        Website_thoi_trangContext db = new Website_thoi_trangContext();
        JwtFilter jwt = new JwtFilter();
        public IActionResult Index()
        {
            try
            {
                if (getDataToken().roleName.Equals("User"))
                {
                    int userId = int.Parse(getDataToken().userId);
                    User user = db.Users.Find(userId);
                    return View(user);
                }
                else
                {
                    return RedirectToAction("Index", "Login", new { });
                }
            }
            catch
            {
                return RedirectToAction("Index", "Login", new { });
            }

        }
        [HttpGet]
        public IActionResult getOder()
        {
            try
            {
                if (getDataToken().roleName.Equals("User"))
                {
                    var lisst = db.Oders.Where(x => x.UserId == 1 && x.Status == false).ToList();
                    string json = JsonSerializer.Serialize(lisst);
                    return Json(json);
                }
                return BadRequest();
            }
            catch {
                return BadRequest();
            }
        }
        [HttpGet]
        public IActionResult getOderDetail(int id)
        {
            try
            {
                if (getDataToken().roleName.Equals("User")|| getDataToken().roleName.Equals("Admin") || getDataToken().roleName.Equals("NhanVien"))
                {
                    var listOder = db.Oders.Where(x => x.OderId == id);
                    var listQuantitie = db.Products.Include(s => s.Quantities.Where(s => s.OderDetails.Count() > 0)).ThenInclude(s => s.OderDetails.Where(s => s.OderId == id && s.quantity > 0)).Where(s => s.Quantities.Count() > 0).ToList();
                    var list = db.OderDetails.Select(s => new
                    {
                        OderDetails = s,
                        Quantities = s.Quantity,
                        Products = s.Quantity.Product,
                    }).Where(s => s.OderDetails.OderId == id).ToList();
                    // var listQuantitie1 = db.OderDetails.Where(s=>s.OderId==id).Select(s=>s.Quantity).ToList();
                    string json = JsonSerializer.Serialize(list);
                    return Json(json);
                }
                    return BadRequest();
                }
            catch
            {
                return BadRequest();
            }
        }
        [HttpGet]
        public IActionResult getInvoice()
        {
            try
            {
                if (getDataToken().roleName.Equals("User"))
                {
                    var lisst = db.Oders.Where(x => x.UserId == 1 && x.Status == true).ToList();
                    string json = JsonSerializer.Serialize(lisst);
                    return Json(json);
                }
                return BadRequest();
            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpPost]
        public IActionResult editUser(User user)
        {
            try
            {
                if (getDataToken().roleName.Equals("User"))
                {
                    int userId = int.Parse(getDataToken().userId);
                    string oldPass = Request.Headers["oldPassword"];
                    User userEdit = db.Users.Find(userId);
                    if (string.IsNullOrEmpty(oldPass)==false )
                    {
                        if (jwt.VerifyPassword(oldPass, userEdit.PassWord))
                        {
                            userEdit.FullName = user.FullName;
                            userEdit.Birthday = user.Birthday;
                            userEdit.PhoneNumber = user.PhoneNumber;
                            userEdit.Email = user.Email;
                            userEdit.Gender = user.Gender;
                            userEdit.Role = "0";
                            int check = int.Parse(Request.Headers["editPassword"]);
                            if (check == 1)
                            {
                                userEdit.PassWord = jwt.HashPassword(user.PassWord);
                            }
                            db.SaveChanges();
                            return Ok();
                        }
                    }
                    return BadRequest();
                }
                return BadRequest();
            }
            catch
            {
                return BadRequest();
            }

        }
        [HttpPost]
        public IActionResult postData()
        {
            try
            {
                if (getDataToken().roleName.Equals("User"))
                {
                    int userId = int.Parse(getDataToken().userId);
                    User user = db.Users.Find(userId);
                    return Json(user);
                }
                return BadRequest();
            }
            catch
            {
                return BadRequest();
            }
        }
    }
   
}
