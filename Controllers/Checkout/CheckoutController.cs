﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Ql_Ban_Hang.Models;
using System.Text.Json;

namespace Ql_Ban_Hang.Controllers.Checkout
{
    public class CheckoutController : BaseController
    {
        Website_thoi_trangContext db = new Website_thoi_trangContext();
        public IActionResult Index()
        {
            try
            {
                if (getDataToken().roleName.Equals("User"))
                {   if(CheckListCart()==false)
                    {
                        return RedirectToAction("Index", "Shop", new { });
                    }
                    int userId = int.Parse(getDataToken().userId);
                    var listCart = db.Carts.Where(x => x.UserId == userId).ToList();
                    var listProduct = new List<Product>();
                    foreach (var item in listCart)
                    {
                        listProduct.Add(db.Products.Include("Quantities").FirstOrDefault(c => c.ProductId == item.ProductId));
                    }

                    return View(listProduct);
                }
                else
                {
                    return RedirectToAction("Index", "Login", new { });
                }
            }
            catch
            {
                return RedirectToAction("Index", "Login", new { });
            }
        }
        public IActionResult getDataOder() {
            try
            {
                if (getDataToken().roleName.Equals("User"))
                {
                    int userId = int.Parse(getDataToken().userId);
                    User user = db.Users.Find(userId);
                    string json = JsonSerializer.Serialize(user);
                    return Json(json);
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }
        public int checkVoucher(string voucher)
        {
            try
            {
                if (getDataToken().roleName.Equals("User"))
                {
                    Voucher voucherCheck = db.Vouchers.FirstOrDefault(x => x.VoucherName.Equals(voucher)==true&&x.Total>0) ;
                    if (voucherCheck != null)
                    {
                        return voucherCheck.Discount;
                    }
                    else { return 0; }
                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                return 0;
            }
        }

        [HttpPost]
        public int getOder(Oder postOder)
        {
            try
            {
                if (getDataToken().roleName.Equals("User"))
                {
                    string voucher = Request.Headers["voucher"];
                    if (voucher.Count() > 0)
                    {
                        var voucherUse = db.Vouchers.FirstOrDefault(x => x.VoucherName.Equals(voucher) == true);
                        voucherUse.Total = voucherUse.Total - 1;
                        db.SaveChanges();
                      
                    }
                    int userId = int.Parse(getDataToken().userId);
                    Oder newOder = new Oder();
                    newOder.Address = postOder.Address;
                    newOder.PhomeNumber = postOder.PhomeNumber;
                    newOder.PriceTotal = postOder.PriceTotal;
                    newOder.Status = false;
                    newOder.DateOder = DateTime.Now;
                    newOder.UserId = userId;
                    db.Oders.Add(newOder);
                    db.SaveChanges();
                    var id = db.Oders.ToList().Last().OderId;
                    return id;

                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                return 0;
            }
        }
        [HttpPost]
        public int postOderDetail(OderDetail oderDetail)
        {
            try
            {
                if (getDataToken().roleName.Equals("User"))
                {
                    int userId = int.Parse(getDataToken().userId);
                    OderDetail oderDetail1 = new OderDetail();
                    oderDetail1.QuantityId = oderDetail.QuantityId;
                    oderDetail.quantity = oderDetail.quantity;
                    oderDetail.OderId = oderDetail.OderId;
                    var quality = db.Quantities.Find(oderDetail.QuantityId);
                    var product = db.Products.Find(quality.ProductId);
                    quality.Quantity1 = quality.Quantity1 - 1;
                    db.OderDetails.Add(oderDetail);
                    db.SaveChanges();
                    product.SoLuong = product.SoLuong - 1;
                    db.SaveChanges();
                    Cart cart = db.Carts.FirstOrDefault(x => x.UserId == userId && x.ProductId == product.ProductId);
                    db.Carts.Remove(cart);
                    db.SaveChanges();
                    return 1;
                }
                else
                { return 0; }
                } 
            catch
            {
                return 0;
            }
        }
        private bool CheckListCart()
        {           
                    int userId = int.Parse(getDataToken().userId);
                    var listCart = db.Carts.Where(x => x.UserId == userId).ToList();
                    var listProduct = new List<Product>();
                    foreach (var item in listCart)
                    {
                        listProduct.Add(db.Products.Find(item.ProductId));
                    }
                     if (listProduct.Count > 0)
                        {
                            return true;
                        }
                    return false;
                }
               
            }
           
        
    }
