﻿using Microsoft.AspNetCore.Mvc;
using Ql_Ban_Hang.Models;
using System.Linq;

namespace Ql_Ban_Hang.Controllers.Admin
{
    public class AdminOderController : BaseController
    {
        Website_thoi_trangContext db = new Website_thoi_trangContext();
        public IActionResult Index()
        {
            try
            {
                if (getDataToken().roleName.Equals("Admin") || getDataToken().roleName.Equals("NhanVien"))
                {
                    var listOder = db.Oders.Where(x => x.Status == false).ToList();
                    return View(listOder);
                }
                return BadRequest();

            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpPost]
        public IActionResult confirmPay(int id)
        {
            try
            {
                if (getDataToken().roleName.Equals("Admin")|| getDataToken().roleName.Equals("NhanVien"))
                {
                    var oder = db.Oders.FirstOrDefault(x => x.OderId == id);
                    oder.Status = true;
                    db.SaveChanges();
                    return Json("Thành công");
                }
                return BadRequest();

            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpPost]
        public IActionResult deleteOder(int id)
        {
            try
            {
                if (getDataToken().roleName.Equals("Admin") || getDataToken().roleName.Equals("NhanVien"))
                {
                    var listOderDetail = db.OderDetails.Where(x => x.OderId == id).ToList();

                    foreach (var index in listOderDetail)
                    {
                        Quantity quantity = db.Quantities.Find(index.QuantityId);
                        Product product = db.Products.Find(quantity.ProductId);
                        product.SoLuong = product.SoLuong + index.quantity;
                        quantity.Quantity1 = quantity.Quantity1 + index.quantity;
                        db.SaveChanges();
                    }
                    foreach (var x in listOderDetail)
                    {
                        db.OderDetails.Remove(x);
                        db.SaveChanges();
                    }
                    Oder oder = db.Oders.Find(id);
                    db.Oders.Remove(oder);
                    db.SaveChanges();
                    return Json("Thành công");
                }
                return BadRequest();

            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
