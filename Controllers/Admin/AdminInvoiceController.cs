﻿using Microsoft.AspNetCore.Mvc;
using Ql_Ban_Hang.Models;

namespace Ql_Ban_Hang.Controllers.Admin
{
    public class AdminInvoiceController : BaseController
    {
        Website_thoi_trangContext db = new Website_thoi_trangContext();
        public IActionResult Index()
        {
            try
            {
                if (getDataToken().roleName.Equals("Admin"))
                {
                    var listOder = db.Oders.Where(x => x.Status == true).ToList();
                    return View(listOder);
                }
                return BadRequest();

            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
