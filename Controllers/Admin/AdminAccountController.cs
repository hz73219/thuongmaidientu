﻿using Microsoft.AspNetCore.Mvc;
using Ql_Ban_Hang.Models;
using Ql_Ban_Hang.Security;

namespace Ql_Ban_Hang.Controllers.Admin
{
    public class AdminAccountController : BaseController
    {
        Website_thoi_trangContext db = new Website_thoi_trangContext();
        JwtFilter jwt = new JwtFilter();
        public IActionResult Index()
        {
            var list = db.Users.Where(s=>s.Role=="1"||s.Role=="2").ToList();
            return View(list);
        }
        [HttpPost]
        public IActionResult DangKyTaiKhoan(User userDK)
        {
            try
            {
                bool kiemTra = kiemTraUserName(userDK.UserName);
                if (kiemTra == false)
                {
                    return Json("Đăng ký không thành công!");
                }
                User user = new User();
                user.Address = userDK.Address;
                user.PhoneNumber = userDK.PhoneNumber;
                user.Email = userDK.Email;
                user.Birthday = userDK.Birthday;
                user.Gender = userDK.Gender;
                user.FullName = userDK.FullName;
                user.UserName = userDK.UserName;
                user.PassWord = jwt.HashPassword(userDK.PassWord);
                user.Role = userDK.Role;
                db.Users.Add(user);
                db.SaveChanges();
                return Json("Đăng ký thành công!");
            }
            catch
            {
                return Json("Đăng ký không thành công1!");
            }
        }
        public bool kiemTraUserName(string username)
        {
            var user = db.Users.Where(x => x.UserName == username).ToList();
            if (user.Count == 0)
            {
                return true;
            }
            else
                return false;
        }
        [HttpPost]
        public IActionResult postData(int id)
        {
            try
            {
                if (getDataToken().roleName.Equals("Admin"))
                {
                    User user = db.Users.Find(id);
                    return Json(user);
                }
                return BadRequest();
            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpPost]
        public IActionResult editUser(User user)
        {
            try
            {
                if (getDataToken().roleName.Equals("Admin"))
                {
                    User userEdit = db.Users.Find(user.UserId);                      
                            userEdit.FullName = user.FullName;
                            userEdit.Birthday = user.Birthday;
                            userEdit.PhoneNumber = user.PhoneNumber;
                            userEdit.Email = user.Email;
                            userEdit.Gender = user.Gender;
                            userEdit.UserName = user.UserName;
                            userEdit.Role = user.Role;
                            int check = int.Parse(Request.Headers["editPassword"]);
                            if (check == 1)
                            {
                                userEdit.PassWord = jwt.HashPassword(user.PassWord);
                            }
                            db.SaveChanges();
                            return Ok();                   
                                    
                }
                return BadRequest();
            }
            catch
            {
                return BadRequest();
            }

        }
        [HttpPost]
        public IActionResult deleteUser(int id)
        {
            try
            {
                int userId = int.Parse(getDataToken().userId);
                if (getDataToken().roleName.Equals("Admin")&& userId!=id)
                {
                    User user = db.Users.Find(id);
                    db.Users.Remove(user);
                    db.SaveChanges();
                    return Ok();
                }
                return BadRequest();
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
