﻿using Microsoft.AspNetCore.Mvc;
using Ql_Ban_Hang.Models;

namespace Ql_Ban_Hang.Controllers.Admin
{
    public class NhanVienController : BaseController
    {
        Website_thoi_trangContext db = new Website_thoi_trangContext();
        public IActionResult Index()
        {
            try
            {
                if ( getDataToken().roleName.Equals("NhanVien"))
                {
                    var listOder = db.Oders.Where(x => x.Status == false).ToList();
                    return View(listOder);
                }
                return BadRequest();

            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
