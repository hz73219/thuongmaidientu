﻿using Microsoft.AspNetCore.Mvc;
using Ql_Ban_Hang.Models;
using System;
using System.Text.Json;

namespace Ql_Ban_Hang.Controllers.Admin
{
    public class AdminVoucherController : BaseController
    {
        Website_thoi_trangContext db = new Website_thoi_trangContext();
        public IActionResult Voucher()
        {
            try
            {
                if (getDataToken().roleName.Equals("Admin"))
                {
                    var listVoucher = db.Vouchers.Where(x => x.Total > 0).ToList();
                    return View(listVoucher);
                }
                return BadRequest();

            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpPost]
        public IActionResult addVoucher(Voucher voucher)
        {
            try
            {
                if (getDataToken().roleName.Equals("Admin"))
                {
                    int random = int.Parse(Request.Headers["Random"]);
                    if (random == 1)
                    {
                        string randomNameVoucher = "";
                        for (int i = 0; i < 6; i++)
                        {
                            Random rnd = new Random();
                            int x = rnd.Next(65, 122);
                            randomNameVoucher = randomNameVoucher + (char)x;
                        }
                        voucher.VoucherName = randomNameVoucher;
                        if (findVoucherName(voucher.VoucherName))
                        {
                            db.Vouchers.Add(voucher);
                            db.SaveChanges();
                            return Json("Thành Công");
                        }
                        else
                        {
                            return Json("Mã voucher đã có!");
                        }
                    }
                    else
                    {
                        if (findVoucherName(voucher.VoucherName))
                        {
                            db.Vouchers.Add(voucher);
                            db.SaveChanges();
                            return Json("Thành Công");
                        }
                        else
                        {
                            return Json("Mã voucher đã có!");
                        }
                    }
                }
                else
                    return Json("Thất bại");

            }
            catch 
            {
                return Json("Thất bại");
            }
        }
        private bool findVoucherName(string voucherName)
        {
            var list = db.Vouchers.ToList();
            var voucher = list.FirstOrDefault(x=>x.VoucherName.Equals(voucherName)==true);
            if(voucher == null)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public IActionResult deleteVoucher(int id)
        {
            if (getDataToken().roleName.Equals("Admin"))
            {
                var voucher = db.Vouchers.Find(id);
                db.Vouchers.Remove(voucher);
                db.SaveChanges();
                return Json("Xóa thành công!");
            }
            else
            {
                return Json("Xóa không thành công!");
            }
        }
        [HttpPost]
        public IActionResult editVoucher(Voucher voucher)
        {
            try
            {
                if (getDataToken().roleName.Equals("Admin"))
                {
                    if (findVoucherName(voucher.VoucherName))
                    {
                        var voucherEdit = db.Vouchers.FirstOrDefault(x => x.VoucherId == voucher.VoucherId);
                        voucherEdit.VoucherName = voucher.VoucherName;
                        voucherEdit.Discount = voucher.Discount;
                        voucherEdit.Total = voucher.Total;
                        db.SaveChanges();
                        return Json("Sửa thành công!");
                    }
                    return Json("Tên voucher đã được dùng!");
                }
                else
                {
                    return Json("Sửa không thành công!");
                }
            }
            catch
            {

                return BadRequest(); 
            }
        }
        public IActionResult getVoucher(int id)
        {
            try
            {
                if (getDataToken().roleName.Equals("Admin"))
                {
                    var voucher = db.Vouchers.Find(id);
                    string json = JsonSerializer.Serialize(voucher);
                    return Json(json);
                }
                return BadRequest();
            }
            catch { return BadRequest(); }
                
        }
    }
}
