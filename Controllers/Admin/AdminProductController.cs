﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using Ql_Ban_Hang.Models;
using Ql_Ban_Hang.Security;
using Website_thoi_trang.Security;
using Ql_Ban_Hang.Controllers;

namespace Website_thoi_trang.Controllers
{
    public class AdminController : BaseController
    {
        Website_thoi_trangContext db = new Website_thoi_trangContext();

        [Route("Admin/Product")]
        public IActionResult Product()
        {
            // check có quyền vào hay không
           try
            {
                UserRole userRole = getDataToken();
                if (!userRole.roleName.Equals("Admin"))
                {
                    return RedirectToAction("Index", "Login", new { });
                }
                var listProdct = db.Products.Where(x => x.SoLuong>0).ToList();
                return View(listProdct);
            }
            catch
            {
                return RedirectToAction("Index", "Login", new { });
            }
        }
        [HttpPost]
        [Route("Admin/addProduct")]
        public IActionResult addProduct(PoductAdd product)
        {
            try
            {
                if (getDataToken().roleName.Equals("Admin"))
                {
                    Product productAdd = new Product();
                    productAdd.SoLuong = product.slSize1+product.slSize2+product.slSize3+product.slSize4;
                    productAdd.NameProduct = product.NameProduct;
                    productAdd.Collection = product.Collection;
                    productAdd.Color = product.Color;
                    productAdd.Brand = product.Brand;
                    productAdd.Price = product.Price;
                    productAdd.Title = product.Title;
                    db.Products.Add(productAdd);
                    db.SaveChanges();
                    var listProdct = db.Products.Where(x => x.SoLuong > 0).ToList();
                    var  list = db.Products.ToList();
                    Product item = list.Last();
                    {
                        Quantity quantily1 = new Quantity();
                        quantily1.Size = 1.ToString();
                        quantily1.ProductId = item.ProductId;
                        quantily1.Quantity1 = product.slSize1;
                        db.Quantities.Add(quantily1);
                        db.SaveChanges();
                    }
                    {
                        Quantity quantily2 = new Quantity();
                        quantily2.Size = 2.ToString();
                        quantily2.ProductId = item.ProductId;
                        quantily2.Quantity1 = product.slSize1;
                        db.Quantities.Add(quantily2);
                        db.SaveChanges();
                    }
                    {
                        Quantity quantily3 = new Quantity();
                        quantily3.Size = 3.ToString();
                        quantily3.ProductId = item.ProductId;
                        quantily3.Quantity1 = product.slSize1;
                        db.Quantities.Add(quantily3);
                        db.SaveChanges();
                    }
                    {
                        Quantity quantily4 = new Quantity();
                        quantily4.Size = 4.ToString();
                        quantily4.ProductId = item.ProductId;
                        quantily4.Quantity1 = product.slSize1;
                        db.Quantities.Add(quantily4);
                        db.SaveChanges();
                    }
                    return Json(item.ProductId);
                }
                return Json(0);
            }
            catch 
            {
                return Json(0);
            }
        }

        [HttpPost]
        [Route("Admin/addImage")]
        public ActionResult addImage(List<IFormFile> files)
        {
            try
            {
                if (getDataToken().roleName.Equals("Admin"))
                {
                    string id = Request.Headers["id"];
                    int idProduct = Int32.Parse(id);
                    string sever = @"D:\server\images\product\";
                    foreach (var formFile in files)
                    {
                        if (formFile.Length > 0)
                        {
                            var filePath = sever + id + ".png";
                            Product addProduct = db.Products.Find(idProduct);
                            if (addProduct != null)
                            {
                                addProduct.Image = @"http:\\serverimage\product\" + id + ".png";
                            }

                            db.SaveChanges();
                            using (var stream = System.IO.File.Create(filePath))
                            {
                                formFile.CopyTo(stream);
                            }
                        }
                    }
                    return Json("Thành công!");
                }
                return Json("Thất bại!");
            }
            catch
            {
                return Json("Thất bại !");
            }

        }
        [HttpPost]
        [Route("Admin/deleteProduct/{id?}")]
        public IActionResult deleteProduct(int id)
        {
            try
            {
                if (getDataToken().roleName.Equals("Admin"))
                {
                    var product = db.Products.Find(id);
                    List<Quantity> listDeleteQuantily = db.Quantities.Where(x => x.ProductId == id).ToList();
                    for (int i = 0; i < 4; i++)
                    {
                        db.Quantities.Remove(listDeleteQuantily[i]);
                        db.SaveChanges();
                    }
                    db.Products.Remove(product);
                    db.SaveChanges();
                    string detelePart = converPart(product.Image);
                    System.IO.File.Delete(detelePart);
                    return Json("Xóa thành công!");
                }
                else
                {
                    return Json("Xóa không thành công!");
                }
            }
            catch { 
                return BadRequest();
            }
        }
        [HttpPost]
        [Route("Admin/editProduct")]
        public ActionResult editProduct(PoductAdd product)
        {
            try
            {
                if (getDataToken().roleName.Equals("Admin"))
                {
                    int id = product.ProductId;
                    var productEdit = db.Products.Find(product.ProductId);
                    productEdit.NameProduct = product.NameProduct;
                    productEdit.Brand = product.Brand;
                    productEdit.Collection = product.Collection;
                    productEdit.Color = product.Color;
                    productEdit.Price = product.Price;
                    productEdit.Title = product.Title;
                    productEdit.SoLuong = product.slSize1+ product.slSize2+ product.slSize3+ product.slSize4;
                    var list = db.Quantities.Where(x => x.ProductId == product.ProductId).ToList();
                    list[0].Quantity1 = product.slSize1;
                    list[1].Quantity1 = product.slSize2;
                    list[2].Quantity1 = product.slSize3;
                    list[3].Quantity1 = product.slSize4;
                    int check = int.Parse(Request.Headers["editImage"]);
                    if (check == 0)
                    {
                        string detelePart = converPart(productEdit.Image);
                        System.IO.File.Delete(detelePart);
                    }
                    db.SaveChanges();

                    return Json(id);
                }
                else
                {
                    return Json(0);
                }
            }
            catch
            {
                return BadRequest();
            }
        }
        public string converPart(string producImage)
        {
            string part = producImage.Substring(26);
            return @"D:\server\images\product\" + part;
        }
        [HttpPost]
        [Route("Admin/postData/{id?}")]
        public IActionResult postData(int id)
        {
           try
            {
                var product = db.Products.Find(id);
                var listQuatiny = db.Quantities.Where(x => x.ProductId == id).ToList();
                PoductAdd poduc = new PoductAdd();
                poduc.ProductId = id;
                poduc.NameProduct = product.NameProduct;
                poduc.Brand = product.Brand;
                poduc.Color = product.Color;
                poduc.Collection = product.Collection;
                poduc.Title = product.Title;
                poduc.Price = product.Price;
                poduc.slSize1 = listQuatiny[0].Quantity1;
                poduc.slSize2 = listQuatiny[1].Quantity1;
                poduc.slSize3 = listQuatiny[2].Quantity1;
                poduc.slSize4 = listQuatiny[3].Quantity1;
                return Json(poduc);
            }
            catch 
            {
                return BadRequest();
            }
        }
    }
}
