﻿using Microsoft.AspNetCore.Mvc;
using Ql_Ban_Hang.Controllers;
using Ql_Ban_Hang.Models;
using Ql_Ban_Hang.Security;

namespace Ql_Ban_Hang.Controllers.Home
{
    public class HomeController : BaseController
    {
        Website_thoi_trangContext db = new Website_thoi_trangContext();

        public IActionResult Index()
        {   try
            {
                if (getDataToken().roleName.Equals("User"))
                {

                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Login", new { });
                }
            }
            catch 
            {
                return RedirectToAction("Index", "Login", new { });
            }
            
        }
    }
}
