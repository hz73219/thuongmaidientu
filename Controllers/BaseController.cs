﻿using Microsoft.AspNetCore.Mvc;
using Ql_Ban_Hang.Models;
using Ql_Ban_Hang.Security;
using Website_thoi_trang.Security;

namespace Ql_Ban_Hang.Controllers
{
    public class BaseController : Controller
    {

        public Website_thoi_trang.Security.UserRole getDataToken()
        {
            string token = Request.Cookies["token"];
            JwtFilter filter = new JwtFilter();
            if(token != null)
            {
                return filter.decryptionJwtToken(token);
            }
            else
            {
                return  new UserRole();
            }
        }
    }
}
