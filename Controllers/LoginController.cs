﻿using Ql_Ban_Hang.Models;
using Ql_Ban_Hang.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using System.Web;
using System.Globalization;

namespace Ql_Ban_Hang.Controllers.Login
{
    public class LoginController : BaseController
    {
        Website_thoi_trangContext db = new Website_thoi_trangContext();
        JwtFilter jwt = new JwtFilter();
      
        public IActionResult Login()
        {      
                return View();        
        }
        public IActionResult Index(string username , string password)
        {
            try
            {
                var userLogin = db.Users.FirstOrDefault(u => u.UserName == username);
                if (userLogin == null)
                {
                    ViewBag.Messeage = "Không tìm thấy tài khoản hoặc mật khẩu";
                    return View();
                }
                else
                {
                    if(jwt.VerifyPassword(password, userLogin.PassWord))
                    {
                        string role = "";
                         if (userLogin.Role.Equals("1"))
                         {
                            role = "Admin";
                            string token = jwt.GenerateJwtToken(userLogin.UserId.ToString(), role);
                            var cookieOptions = new CookieOptions();
                            cookieOptions.Expires = DateTime.Now.AddDays(7);
                            cookieOptions.Path = "/";
                            Response.Cookies.Append("token", token, cookieOptions);
                            return RedirectToAction("Product", "Admin", new {});
                         }
                        if (userLogin.Role.Equals("0"))
                        { 
                            role = "User";
                            string token = jwt.GenerateJwtToken(userLogin.UserId.ToString(), role);
                            var cookieOptions = new CookieOptions();
                            cookieOptions.Expires = DateTime.Now.AddDays(7);
                            cookieOptions.Path = "/";
                            Response.Cookies.Append("token", token, cookieOptions);
                            return RedirectToAction("Index", "Home", new { });
                        }
                        else
                        {
                            role = "NhanVien";
                            string token = jwt.GenerateJwtToken(userLogin.UserId.ToString(), role);
                            var cookieOptions = new CookieOptions();
                            cookieOptions.Expires = DateTime.Now.AddDays(7);
                            cookieOptions.Path = "/";
                            Response.Cookies.Append("token", token, cookieOptions);
                            return RedirectToAction("Index", "NhanVien", new { });
                        }

                    }
                    ViewBag.Messeage = "Không tìm thấy tài khoản hoặc mật khẩu";
                    return View();
                }
            }
            catch
            {
                return View();
            }
        }
        public IActionResult DangKy()
        {
            return View();
        }
        [HttpPost]
        public IActionResult DangKyTaiKhoan(User userDK)
        {
            try
            {
                bool kiemTra = kiemTraUserName(userDK.UserName);
                if (kiemTra == false)
                {
                    return Json("Đăng ký không thành công!");
                }             
                User user = new User();
                user.Address = userDK.Address;
                user.PhoneNumber = userDK.PhoneNumber;
                user.Email = userDK.Email;
                user.Birthday = userDK.Birthday;
                user.Gender = userDK.Gender;
                user.FullName = userDK.FullName;
                user.UserName = userDK.UserName;
                user.PassWord = jwt.HashPassword(userDK.PassWord);
                user.Role = "0";
                db.Users.Add(user);
                db.SaveChanges();
                return Json("Đăng ký thành công!");
            }
            catch 
            {
                return Json("Đăng ký không thành công1!");
            }
        }
        public bool kiemTraUserName(string username)
        {
            var user = db.Users.Where(x => x.UserName== username).ToList();
            if (user.Count == 0)
            {
                return true;
            }
            else
                return false;
        }
        public IActionResult logOut()
        {
            Response.Cookies.Delete("token");
            return RedirectToAction("Index", "Login", new { });
        }
    }
}
