﻿using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using Ql_Ban_Hang.Models;
using Ql_Ban_Hang.Security;

using Website_thoi_trang.Security;
using System.Text.Json.Serialization;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;

namespace Ql_Ban_Hang.Controllers.Shop
{
    public static class QueryHelper
    {
        public static IQueryable<T> GetPage<T>(this IQueryable<T> source, int pageNumber, int pageSize) where T : class
        {
            return source.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }      
    }
    public class FilterCore
    {
        public int page { get; set; } = 1;
        public int pageSize { get; set; } = 9;
    }

    public class Filter : FilterCore
    {
        public string searchTerm { get; set; } = "";
        public string type { get; set; } = "";
    }

    public class ShopController : BaseController
    {
        Website_thoi_trangContext db = new Website_thoi_trangContext();
        [HttpGet]
        public JsonResult getListCart()
        {
            try
            {
                if (getDataToken().roleName.Equals("User"))
                {
                    int userId = int.Parse(getDataToken().userId);
                    var listCart = db.Carts.Where(x => x.UserId == userId).ToList();
                    var listProduct = new List<Product>();
                    foreach (var item in listCart)
                    {
                        listProduct.Add(db.Products.Find(item.ProductId));
                    }

                    string json = JsonSerializer.Serialize(listProduct);
                    return Json(json);
                }
                else
                {
                    return Json("Vui lòng đăng nhập");
                }
            }
            catch
            {
                return Json("Vui lòng đăng nhập");
            }
        }

        //static 
        public IActionResult Index()
        {
            try
            {
                if (getDataToken().roleName.Equals("User"))
                {
                    Filter filter = new Filter();
                    @ViewBag.Find = "Shop";
                    ViewBag.Page = 1;
                    ViewBag.LastPage = db.Products.Count() / filter.pageSize + 1;
                    ViewBag.AllProduct = db.Products.Count();

                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Login", new { });
                }
            }
            catch
            {
                return RedirectToAction("Index", "Login", new { });
            }
        }
        [HttpPost]
        public JsonResult getListProduct(Filter filter)
        {
            IQueryable<Product> queryProduct = db.Products.Where(x => x.SoLuong>0);    
            if (filter.type.Equals("Collection"))
            {
                queryProduct = queryProduct.Where(product => product.Collection.Contains(filter.searchTerm));
            }
            if (filter.type.Equals("Name"))
            {
                queryProduct = queryProduct.Where(product => product.NameProduct.Contains(filter.searchTerm));
            }
            if (filter.type.Equals("Price")&& filter.searchTerm.Equals("tang"))
            {
                queryProduct = queryProduct.OrderBy(product => product.Price);
            }
            if (filter.type.Equals("Price") && filter.searchTerm.Equals("giam"))
            {
                queryProduct = queryProduct.OrderByDescending(product => product.Price);
            }
            if (filter.type.Equals("Color"))
            {
                queryProduct = queryProduct.Where(product => product.Color.Contains(filter.searchTerm));
            }
            if (filter.type.Equals("Brand"))
            {
                queryProduct = queryProduct.Where(product => product.Brand.Contains(filter.searchTerm));
            }
            List<Product> list = queryProduct.GetPage(filter.page,filter.pageSize).ToList();
            string json = JsonSerializer.Serialize(list);

            int totalItems = queryProduct.Count();
            int totalPages = (int)Math.Ceiling((decimal)totalItems / filter.pageSize);
            if(filter.page > totalPages) {
                filter.page = totalPages;
                if(queryProduct.Count()==0)
                {
                    List<Product> products = new List<Product>();
                    json = JsonSerializer.Serialize(products);
                }
              
            }
            // Pass data to view
            ViewBag.SearchTerm = filter.searchTerm;
            ViewBag.Page = filter.page;
            ViewBag.TotalPages = totalPages;
            ViewBag.TotalItems = totalItems;

            ViewBag.pagesize = filter.pageSize;
            ViewBag.page = filter.page;
            ViewBag.pageNext = filter.page + 1;
            ViewBag.pagePrevios = filter.page - 1;
            ViewBag.LastPage = db.Products.Count() / filter.pageSize + 1;
            return Json(json);
        }
        [HttpPost]
        public IActionResult getCart(int id)
        {    try
            {

                UserRole userRole = getDataToken();
                if (userRole.roleName.Equals("User"))
                {                
                    int idUser = int.Parse(userRole.userId);
                    var listCart = db.Carts.Where(x => x.UserId == idUser).ToList();
                    foreach(var item in listCart)
                    {
                        if (item.ProductId==id)
                        {
                            return Json("Đã có trong giỏ hàng!");
                        }
                    }
                    Cart cartAdd = new Cart();
                    cartAdd.UserId = idUser;
                    cartAdd.ProductId = id;
                    db.Carts.Add(cartAdd);
                    db.SaveChanges();
                    return Json("Them gio hang cong");
                }
                return RedirectToAction("Index", "Login", new { });
            }
            catch
            {
                return RedirectToAction("Index", "Login", new { });
            }
        }
        [HttpPost]
        public IActionResult removeCart(int id)
        {
            try
            {

                UserRole userRole = getDataToken();
                if (userRole.roleName.Equals("User"))
                {
                    int idUser = int.Parse(userRole.userId);
                    var listCart = db.Carts.Where(x => x.UserId == idUser).ToList();
                    foreach (var item in listCart)
                    {
                        if (item.ProductId == id)
                        {
                            db.Carts.Remove(item);
                            db.SaveChanges();
                            return Json("Xóa thành công!");
                        }
                    }
                }
                return RedirectToAction("Index", "Login", new { });
            }
            catch
            {
                return RedirectToAction("Index", "Login", new { });
            }
        }
        public IActionResult Details(int id)
        {
            try
            {
                if (getDataToken().roleName.Equals("User"))
                {
                    var product = db.Products.Find(id);
                    return View(product);
                }
                else
                {
                    return RedirectToAction("Index", "Login", new { });
                }
            }
            catch
            {
                return RedirectToAction("Index", "Login", new { });
            }
        }

    }
}
