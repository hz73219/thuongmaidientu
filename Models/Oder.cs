﻿using System;
using System.Collections.Generic;

namespace Ql_Ban_Hang.Models
{
    public partial class Oder
    {
        public Oder()
        {
            OderDetails = new HashSet<OderDetail>();
        }

        public int OderId { get; set; }
        public int UserId { get; set; }
        public string? Address { get; set; }
        public bool? Status { get; set; }
        public DateTime? DateOder { get; set; }
        public DateTime? DateInvoie { get; set; }
        public int? PriceTotal { get; set; }
        public string? PhomeNumber { get; set; }
        public virtual User User { get; set; } = null!;
        public virtual ICollection<OderDetail> OderDetails { get; set; }
    }
}
