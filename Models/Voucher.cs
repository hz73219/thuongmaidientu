﻿using System;
using System.Collections.Generic;

namespace Ql_Ban_Hang.Models
{
    public partial class Voucher
    {
        public int VoucherId { get; set; }
        public string VoucherName { get; set; } = null!;
        public int Discount { get; set; }
        public int? Total { get; set; }
    }
}
