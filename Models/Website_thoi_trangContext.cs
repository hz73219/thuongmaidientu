﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Ql_Ban_Hang.Models
{
    public partial class Website_thoi_trangContext : DbContext
    {
        public Website_thoi_trangContext()
        {
        }

        public Website_thoi_trangContext(DbContextOptions<Website_thoi_trangContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cart> Carts { get; set; } = null!;
        public virtual DbSet<Oder> Oders { get; set; } = null!;
        public virtual DbSet<OderDetail> OderDetails { get; set; } = null!;
        public virtual DbSet<Product> Products { get; set; } = null!;
        public virtual DbSet<Quantity> Quantities { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;
        public virtual DbSet<Voucher> Vouchers { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server= LAPTOP-6791LU43;Database= Website_thoi_trang; user id=sa;password=12;Trusted_Connection=True;TrustServerCertificate=True\n");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cart>(entity =>
            {
                entity.ToTable("Cart");

                entity.Property(e => e.CartId).HasColumnName("cartId");

                entity.Property(e => e.ProductId).HasColumnName("productId");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Carts)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Cart_Product");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Carts)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Cart_User");
            });

            modelBuilder.Entity<Oder>(entity =>
            {
                entity.ToTable("Oder");

                entity.Property(e => e.OderId).HasColumnName("oderId");

                entity.Property(e => e.Address)
                    .HasMaxLength(300)
                    .HasColumnName("address");
                entity.Property(e => e.PhomeNumber)
                   .HasMaxLength(10)
                   .HasColumnName("phomeNumber");

                entity.Property(e => e.DateInvoie)
                    .HasColumnType("date")
                    .HasColumnName("dateInvoie");

                entity.Property(e => e.DateOder)
                    .HasColumnType("date")
                    .HasColumnName("dateOder");

                entity.Property(e => e.PriceTotal).HasColumnName("priceTotal");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Oders)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Oder_User");
            });

            modelBuilder.Entity<OderDetail>(entity =>
            {
                entity.ToTable("OderDetail");

                entity.Property(e => e.OderDetailId).HasColumnName("oderDetailId");

                entity.Property(e => e.OderId).HasColumnName("oderId");

                entity.Property(e => e.quantity).HasColumnName("quantity");

                entity.Property(e => e.QuantityId).HasColumnName("quantityId");


                entity.HasOne(d => d.Oder)
                    .WithMany(p => p.OderDetails)
                    .HasForeignKey(d => d.OderId)
                    .HasConstraintName("FK_OderDetail_Oder");

                entity.HasOne(d => d.Quantity)
                    .WithMany(p => p.OderDetails)
                    .HasForeignKey(d => d.QuantityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OderDetail_Quantity");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("Product");

                entity.Property(e => e.ProductId).HasColumnName("productId");

                entity.Property(e => e.Brand)
                    .HasMaxLength(50)
                    .HasColumnName("brand");

                entity.Property(e => e.Collection)
                    .HasMaxLength(50)
                    .HasColumnName("collection");

                entity.Property(e => e.Color)
                    .HasMaxLength(10)
                    .HasColumnName("color")
                    .IsFixedLength();

                entity.Property(e => e.Image)
                    .HasMaxLength(150)
                    .HasColumnName("image");

                entity.Property(e => e.NameProduct)
                    .HasMaxLength(50)
                    .HasColumnName("nameProduct");

                entity.Property(e => e.Price).HasColumnName("price");
                entity.Property(e => e.SoLuong).HasColumnName("soLuong");

                entity.Property(e => e.Title)
                    .HasMaxLength(500)
                    .HasColumnName("title");
            });

            modelBuilder.Entity<Quantity>(entity =>
            {
                entity.ToTable("Quantity");

                entity.Property(e => e.QuantityId).HasColumnName("quantityId");

                entity.Property(e => e.ProductId).HasColumnName("productId");

                entity.Property(e => e.Quantity1).HasColumnName("quantity");

                entity.Property(e => e.Size)
                    .HasMaxLength(5)
                    .HasColumnName("size")
                    .IsFixedLength();

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Quantities)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Quantity_Product");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.Property(e => e.Address)
                    .HasMaxLength(200)
                    .HasColumnName("address");

                entity.Property(e => e.Birthday)
                    .HasColumnType("date")
                    .HasColumnName("birthday");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email");

                entity.Property(e => e.FullName)
                    .HasMaxLength(50)
                    .HasColumnName("fullName");

                entity.Property(e => e.Gender).HasColumnName("gender");

                entity.Property(e => e.PassWord)
                    .HasMaxLength(200)
                    .HasColumnName("passWord");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(10)
                    .HasColumnName("phoneNumber")
                    .IsFixedLength();

                entity.Property(e => e.Role)
                    .HasMaxLength(10)
                    .HasColumnName("role")
                    .IsFixedLength();

                entity.Property(e => e.UserName)
                    .HasMaxLength(16)
                    .HasColumnName("userName")
                    .IsFixedLength();
            });

            modelBuilder.Entity<Voucher>(entity =>
            {
                entity.ToTable("Voucher");

                entity.Property(e => e.VoucherId).HasColumnName("voucherId");

                entity.Property(e => e.Discount).HasColumnName("discount");


                entity.Property(e => e.Total)
                    .HasColumnName("total")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.VoucherName)
                    .HasMaxLength(6)
                    .HasColumnName("voucherName")
                    .IsFixedLength();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
