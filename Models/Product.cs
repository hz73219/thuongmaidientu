﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Ql_Ban_Hang.Models
{
    public partial class Product
    {
        public Product()
        {
            Carts = new HashSet<Cart>();
            Quantities = new HashSet<Quantity>();
        }

        public int ProductId { get; set; }
        public string? NameProduct { get; set; }
        public string? Collection { get; set; }
        public string? Color { get; set; }
        public int Price { get; set; }
        public string? Brand { get; set; }
        public string? Image { get; set; }
        public string? Title { get; set; }
        public int? SoLuong { get; set; }
        [JsonIgnore]
        public virtual ICollection<Cart> Carts { get; set; }
        [JsonIgnore]
        public virtual ICollection<Quantity> Quantities { get; set; }
    }
}
