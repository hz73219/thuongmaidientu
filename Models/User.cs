﻿using System;
using System.Collections.Generic;

namespace Ql_Ban_Hang.Models
{
    public partial class User
    {
        public User()
        {
            Carts = new HashSet<Cart>();
            Oders = new HashSet<Oder>();
        }

        public int UserId { get; set; }
        public string? FullName { get; set; }
        public DateTime Birthday { get; set; }
        public string? Address { get; set; }
        public string? PhoneNumber { get; set; }
        public bool? Gender { get; set; }
        public string? Email { get; set; }
        public string? UserName { get; set; }
        public string? PassWord { get; set; }
        public string? Role { get; set; }

        public virtual ICollection<Cart> Carts { get; set; }
        public virtual ICollection<Oder> Oders { get; set; }
    }
}
