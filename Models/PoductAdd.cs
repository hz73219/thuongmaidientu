﻿namespace Ql_Ban_Hang.Models
{
    public class PoductAdd
    {
        public int ProductId { get; set; }
        public string? NameProduct { get; set; }
        public string? Collection { get; set; }
        public string? Color { get; set; }
        public int Price { get; set; }
        public string? Brand { get; set; }
        public string? Image { get; set; }
        public string? Title { get; set; }
        public int SoLuong { get; set; }
        public int slSize1 { get; set; }
        public int slSize2 { get; set; }
        public int slSize3 { get; set; }
        public int slSize4 { get; set; }
    }
}
