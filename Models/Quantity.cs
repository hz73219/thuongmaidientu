﻿using System;
using System.Collections.Generic;

namespace Ql_Ban_Hang.Models
{
    public partial class Quantity
    {
        public Quantity()
        {
            OderDetails = new HashSet<OderDetail>();
        }

        public int QuantityId { get; set; }
        public string Size { get; set; } = null!;
        public int ProductId { get; set; }
        public int Quantity1 { get; set; }

        public virtual Product Product { get; set; } = null!;
        public virtual ICollection<OderDetail> OderDetails { get; set; }
    }
}
