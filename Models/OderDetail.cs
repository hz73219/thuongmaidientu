﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Ql_Ban_Hang.Models
{
    public partial class OderDetail
    {
        public int OderDetailId { get; set; }
        public int QuantityId { get; set; }
        public int? OderId { get; set; }
        public int quantity { get; set; }
        public virtual Oder? Oder { get; set; }
        [JsonIgnore]
        public virtual Quantity Quantity { get; set; } = null!;
    }
}
